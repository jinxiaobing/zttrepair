import 'package:fluttertoast/fluttertoast.dart';

class ToastUtils {
  static void showShort(String s) {
    Fluttertoast.showToast(
        msg: s.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER);
  }
}
