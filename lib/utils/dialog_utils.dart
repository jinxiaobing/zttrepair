import 'package:flutter/material.dart';

class DialogUtils {
  static void showConfirmDialog(
      BuildContext context, String content, Function confirmCallBack) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("提示"),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("取消")),
              FlatButton(onPressed: confirmCallBack, child: new Text("确定"))
            ],
          );
        });
  }
}
