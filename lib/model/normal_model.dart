import 'package:json_annotation/json_annotation.dart';

part 'normal_model.g.dart';

@JsonSerializable()
class NormalModel extends Object {
  @JsonKey(name: 'result')
  String result;

  @JsonKey(name: 'msg')
  String msg;

  NormalModel(
    this.result,
    this.msg,
  );

  factory NormalModel.fromJson(Map<String, dynamic> srcJson) =>
      _$NormalModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$NormalModelToJson(this);
}
