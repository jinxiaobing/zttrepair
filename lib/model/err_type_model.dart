import 'package:json_annotation/json_annotation.dart';

part 'err_type_model.g.dart';

@JsonSerializable()
class ErrTypeModel extends Object {
  @JsonKey(name: 'TYPE')
  String tYPE;

  @JsonKey(name: 'SUCCESSED')
  String sUCCESSED;

  @JsonKey(name: 'ERROR_MSG')
  String eRRORMSG;

  @JsonKey(name: 'ERR_TYPE_INFO')
  List<ERR_TYPE_INFO> eRRTYPEINFO;

  @JsonKey(name: 'SuccessFlag')
  String successFlag;

  @JsonKey(name: 'ErrorMsg')
  String errorMsg;

  ErrTypeModel(
    this.tYPE,
    this.sUCCESSED,
    this.eRRORMSG,
    this.eRRTYPEINFO,
    this.successFlag,
    this.errorMsg,
  );

  factory ErrTypeModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ErrTypeModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ErrTypeModelToJson(this);
}

@JsonSerializable()
class ERR_TYPE_INFO extends Object {
  @JsonKey(name: 'ERR_TYPE')
  String eRRTYPE;

  @JsonKey(name: 'ERR_TYPE_DESC')
  String eRRTYPEDESC;

  ERR_TYPE_INFO(
    this.eRRTYPE,
    this.eRRTYPEDESC,
  );

  factory ERR_TYPE_INFO.fromJson(Map<String, dynamic> srcJson) =>
      _$ERR_TYPE_INFOFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ERR_TYPE_INFOToJson(this);
}
