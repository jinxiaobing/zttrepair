import 'package:json_annotation/json_annotation.dart';

part 'repair_list_model.g.dart';

List<RepairListModel> getRepairListModelList(List<dynamic> list) {
  List<RepairListModel> result = [];
  list.forEach((item) {
    result.add(RepairListModel.fromJson(item));
  });
  return result;
}

@JsonSerializable()
class RepairListModel extends Object {
  @JsonKey(name: 'RowNumber')
  String rowNumber;

  @JsonKey(name: 'repairId')
  String repairId;

  @JsonKey(name: 'reportPeopleID')
  String reportPeopleID;

  @JsonKey(name: 'reportPeople')
  String reportPeople;

  @JsonKey(name: 'reportTime')
  String reportTime;

  @JsonKey(name: 'repairTime')
  String repairTime;

  @JsonKey(name: 'repairComplete')
  String repairComplete;

  @JsonKey(name: 'intervalTime')
  String intervalTime;

  @JsonKey(name: 'engineers')
  String engineers;

  @JsonKey(name: 'engineerCode')
  String engineerCode;

  @JsonKey(name: 'newPart')
  String newPart;

  @JsonKey(name: 'orderId')
  String orderId;

  @JsonKey(name: 'equipmentCode')
  String equipmentCode;

  @JsonKey(name: 'equipmentName')
  String equipmentName;

  @JsonKey(name: 'maintenOrgan')
  String maintenOrgan;

  @JsonKey(name: 'exceptDescription')
  String exceptDescription;

  @JsonKey(name: 'equipmentNameGroup')
  String equipmentNameGroup;

  RepairListModel(
    this.rowNumber,
    this.repairId,
    this.reportPeopleID,
    this.reportPeople,
    this.reportTime,
    this.repairTime,
    this.repairComplete,
    this.intervalTime,
    this.engineers,
    this.engineerCode,
    this.newPart,
    this.orderId,
    this.equipmentCode,
    this.equipmentName,
    this.maintenOrgan,
    this.exceptDescription,
    this.equipmentNameGroup,
  );

  factory RepairListModel.fromJson(Map<String, dynamic> srcJson) =>
      _$RepairListModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$RepairListModelToJson(this);
}
