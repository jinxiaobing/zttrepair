// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginModel _$LoginModelFromJson(Map<String, dynamic> json) {
  return LoginModel(
    json['username'] as String,
    json['realname'] as String,
    json['authority'] as String,
    json['site'] as String,
    json['siteKey'] as String,
    json['jobTitle'] as String,
    json['result'] as String,
    json['msg'] as String,
  );
}

Map<String, dynamic> _$LoginModelToJson(LoginModel instance) =>
    <String, dynamic>{
      'username': instance.username,
      'realname': instance.realname,
      'authority': instance.authority,
      'site': instance.site,
      'siteKey': instance.siteKey,
      'jobTitle': instance.jobTitle,
      'result': instance.result,
      'msg': instance.msg,
    };
