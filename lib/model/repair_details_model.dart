import 'package:json_annotation/json_annotation.dart';

part 'repair_details_model.g.dart';

@JsonSerializable()
class RepairDetailsModel extends Object {
  @JsonKey(name: 'result')
  String result;

  @JsonKey(name: 'msg')
  String msg;

  @JsonKey(name: 'data')
  Data data;

  RepairDetailsModel(
    this.result,
    this.msg,
    this.data,
  );

  factory RepairDetailsModel.fromJson(Map<String, dynamic> srcJson) =>
      _$RepairDetailsModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$RepairDetailsModelToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'equipmentCode')
  String equipmentCode;

  @JsonKey(name: 'equipmentNameGroup')
  String equipmentNameGroup;

  @JsonKey(name: 'equipmentNameGroupId')
  String equipmentNameGroupId;

  @JsonKey(name: 'exceptDescription')
  String exceptDescription;

  @JsonKey(name: 'intervalTime')
  String intervalTime;

  @JsonKey(name: 'maintenOrgan')
  String maintenOrgan;

  @JsonKey(name: 'orderId')
  String orderId;

  @JsonKey(name: 'repairComplete')
  String repairComplete;

  @JsonKey(name: 'repairId')
  String repairId;

  @JsonKey(name: 'repairTime')
  String repairTime;

  @JsonKey(name: 'reportPeople')
  String reportPeople;

  @JsonKey(name: 'reportPeopleID')
  String reportPeopleID;

  @JsonKey(name: 'reportTime')
  String reportTime;

  @JsonKey(name: 'responsePeopleId')
  String responsePeopleId;

  @JsonKey(name: 'source')
  String source;

  @JsonKey(name: 'status')
  String status;

  @JsonKey(name: 'process')
  List<Process> process;

  @JsonKey(name: 'workReport')
  WorkReport workReport;

  Data(
    this.equipmentCode,
    this.equipmentNameGroup,
    this.equipmentNameGroupId,
    this.exceptDescription,
    this.intervalTime,
    this.maintenOrgan,
    this.orderId,
    this.repairComplete,
    this.repairId,
    this.repairTime,
    this.reportPeople,
    this.reportPeopleID,
    this.reportTime,
    this.responsePeopleId,
    this.source,
    this.status,
    this.process,
    this.workReport,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Process extends Object {
  @JsonKey(name: 'createTime')
  String createTime;

  @JsonKey(name: 'operationLog')
  String operationLog;

  Process(
    this.createTime,
    this.operationLog,
  );

  factory Process.fromJson(Map<String, dynamic> srcJson) =>
      _$ProcessFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ProcessToJson(this);
}

@JsonSerializable()
class WorkReport extends Object {
  @JsonKey(name: 'errorType')
  String errorType;

  @JsonKey(name: 'executedActivity')
  String executedActivity;

  @JsonKey(name: 'faultSymptoms')
  String faultSymptoms;

  @JsonKey(name: 'reason')
  String reason;

  @JsonKey(name: 'reasonDetails')
  String reasonDetails;

  @JsonKey(name: 'workDetails')
  String workDetails;

  WorkReport(
    this.errorType,
    this.executedActivity,
    this.faultSymptoms,
    this.reason,
    this.reasonDetails,
    this.workDetails,
  );

  factory WorkReport.fromJson(Map<String, dynamic> srcJson) =>
      _$WorkReportFromJson(srcJson);

  Map<String, dynamic> toJson() => _$WorkReportToJson(this);
}
