import 'package:json_annotation/json_annotation.dart';

part 'login_model.g.dart';

@JsonSerializable()
class LoginModel extends Object {
  @JsonKey(name: 'username')
  String username;

  @JsonKey(name: 'realname')
  String realname;

  @JsonKey(name: 'authority')
  String authority;

  @JsonKey(name: 'site')
  String site;

  @JsonKey(name: 'siteKey')
  String siteKey;

  @JsonKey(name: 'jobTitle')
  String jobTitle;

  @JsonKey(name: 'result')
  String result;

  @JsonKey(name: 'msg')
  String msg;

  LoginModel(
    this.username,
    this.realname,
    this.authority,
    this.site,
    this.siteKey,
    this.jobTitle,
    this.result,
    this.msg,
  );

  factory LoginModel.fromJson(Map<String, dynamic> srcJson) =>
      _$LoginModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$LoginModelToJson(this);
}
