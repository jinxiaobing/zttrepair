import 'package:json_annotation/json_annotation.dart';

part 'children_by_parent_model.g.dart';

@JsonSerializable()
class ChildrenByParentModel extends Object {
  @JsonKey(name: 'result')
  String result;

  @JsonKey(name: 'msg')
  String msg;

  @JsonKey(name: 'data')
  List<Data> data;

  ChildrenByParentModel(
    this.result,
    this.msg,
    this.data,
  );

  factory ChildrenByParentModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ChildrenByParentModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ChildrenByParentModelToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'MCH_CODE')
  String mCHCODE;

  @JsonKey(name: 'MCH_NAME')
  String mCHNAME;

  @JsonKey(name: 'CONTRACT')
  String cONTRACT;

  @JsonKey(name: 'SUP_MCH_CODE')
  String sUPMCHCODE;

  @JsonKey(name: 'SUP_CONTRACT')
  String sUPCONTRACT;

  @JsonKey(name: 'OBJ_LEVEL')
  String oBJLEVEL;

  @JsonKey(name: 'ITEM_CLASS_ID')
  String iTEMCLASSID;

  Data(
    this.mCHCODE,
    this.mCHNAME,
    this.cONTRACT,
    this.sUPMCHCODE,
    this.sUPCONTRACT,
    this.oBJLEVEL,
    this.iTEMCLASSID,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}
