// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'err_type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ErrTypeModel _$ErrTypeModelFromJson(Map<String, dynamic> json) {
  return ErrTypeModel(
    json['TYPE'] as String,
    json['SUCCESSED'] as String,
    json['ERROR_MSG'] as String,
    (json['ERR_TYPE_INFO'] as List)
        ?.map((e) => e == null
            ? null
            : ERR_TYPE_INFO.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['SuccessFlag'] as String,
    json['ErrorMsg'] as String,
  );
}

Map<String, dynamic> _$ErrTypeModelToJson(ErrTypeModel instance) =>
    <String, dynamic>{
      'TYPE': instance.tYPE,
      'SUCCESSED': instance.sUCCESSED,
      'ERROR_MSG': instance.eRRORMSG,
      'ERR_TYPE_INFO': instance.eRRTYPEINFO,
      'SuccessFlag': instance.successFlag,
      'ErrorMsg': instance.errorMsg,
    };

ERR_TYPE_INFO _$ERR_TYPE_INFOFromJson(Map<String, dynamic> json) {
  return ERR_TYPE_INFO(
    json['ERR_TYPE'] as String,
    json['ERR_TYPE_DESC'] as String,
  );
}

Map<String, dynamic> _$ERR_TYPE_INFOToJson(ERR_TYPE_INFO instance) =>
    <String, dynamic>{
      'ERR_TYPE': instance.eRRTYPE,
      'ERR_TYPE_DESC': instance.eRRTYPEDESC,
    };
