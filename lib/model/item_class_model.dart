import 'package:json_annotation/json_annotation.dart';

part 'item_class_model.g.dart';

@JsonSerializable()
class ItemClassModel extends Object {
  @JsonKey(name: 'PKG_INFO')
  List<PKG_INFO> pKGINFO;

  @JsonKey(name: 'CHK_INFO')
  List<CHK_INFO> cHKINFO;

  @JsonKey(name: 'LOGIC_EQUIPMENT_INFO')
  List<LOGIC_EQUIPMENT_INFO> lOGICEQUIPMENTINFO;

  @JsonKey(name: 'SuccessFlag')
  String successFlag;

  @JsonKey(name: 'ErrorMsg')
  String errorMsg;

  ItemClassModel(
    this.pKGINFO,
    this.cHKINFO,
    this.lOGICEQUIPMENTINFO,
    this.successFlag,
    this.errorMsg,
  );

  factory ItemClassModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ItemClassModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ItemClassModelToJson(this);
}

@JsonSerializable()
class PKG_INFO extends Object {
  @JsonKey(name: 'EAM_TYPE')
  String eAMTYPE;

  @JsonKey(name: 'SUCCESSED')
  String sUCCESSED;

  @JsonKey(name: 'ERROR_MSG')
  String eRRORMSG;

  PKG_INFO(
    this.eAMTYPE,
    this.sUCCESSED,
    this.eRRORMSG,
  );

  factory PKG_INFO.fromJson(Map<String, dynamic> srcJson) =>
      _$PKG_INFOFromJson(srcJson);

  Map<String, dynamic> toJson() => _$PKG_INFOToJson(this);
}

@JsonSerializable()
class CHK_INFO extends Object {
  @JsonKey(name: 'TOTAL_LOGIC_EQUIPMENT')
  int tOTALLOGICEQUIPMENT;

  CHK_INFO(
    this.tOTALLOGICEQUIPMENT,
  );

  factory CHK_INFO.fromJson(Map<String, dynamic> srcJson) =>
      _$CHK_INFOFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CHK_INFOToJson(this);
}

@JsonSerializable()
class LOGIC_EQUIPMENT_INFO extends Object {
  @JsonKey(name: 'MCH_CODE')
  String mCHCODE;

  @JsonKey(name: 'MCH_NAME')
  String mCHNAME;

  @JsonKey(name: 'CONTRACT')
  String cONTRACT;

  @JsonKey(name: 'SUP_MCH_CODE')
  String sUPMCHCODE;

  @JsonKey(name: 'SUP_CONTRACT')
  String sUPCONTRACT;

  @JsonKey(name: 'OBJ_LEVEL')
  String oBJLEVEL;

  @JsonKey(name: 'ITEM_CLASS_ID')
  String iTEMCLASSID;

  LOGIC_EQUIPMENT_INFO(
    this.mCHCODE,
    this.mCHNAME,
    this.cONTRACT,
    this.sUPMCHCODE,
    this.sUPCONTRACT,
    this.oBJLEVEL,
    this.iTEMCLASSID,
  );

  factory LOGIC_EQUIPMENT_INFO.fromJson(Map<String, dynamic> srcJson) =>
      _$LOGIC_EQUIPMENT_INFOFromJson(srcJson);

  Map<String, dynamic> toJson() => _$LOGIC_EQUIPMENT_INFOToJson(this);
}
