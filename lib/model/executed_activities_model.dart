import 'package:json_annotation/json_annotation.dart';

part 'executed_activities_model.g.dart';

@JsonSerializable()
class ExecutedActivitiesModel extends Object {
  @JsonKey(name: 'TYPE')
  String tYPE;

  @JsonKey(name: 'SUCCESSED')
  String sUCCESSED;

  @JsonKey(name: 'ERROR_MSG')
  String eRRORMSG;

  @JsonKey(name: 'ACTION_INFO')
  List<ACTION_INFO> aCTIONINFO;

  @JsonKey(name: 'SuccessFlag')
  String successFlag;

  @JsonKey(name: 'ErrorMsg')
  String errorMsg;

  ExecutedActivitiesModel(
    this.tYPE,
    this.sUCCESSED,
    this.eRRORMSG,
    this.aCTIONINFO,
    this.successFlag,
    this.errorMsg,
  );

  factory ExecutedActivitiesModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ExecutedActivitiesModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ExecutedActivitiesModelToJson(this);
}

@JsonSerializable()
class ACTION_INFO extends Object {
  @JsonKey(name: 'PERFORMED_ACTION_ID')
  String pERFORMEDACTIONID;

  @JsonKey(name: 'DESCRIPTION')
  String dESCRIPTION;

  ACTION_INFO(
    this.pERFORMEDACTIONID,
    this.dESCRIPTION,
  );

  factory ACTION_INFO.fromJson(Map<String, dynamic> srcJson) =>
      _$ACTION_INFOFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ACTION_INFOToJson(this);
}
