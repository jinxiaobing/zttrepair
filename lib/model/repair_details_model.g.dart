// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repair_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RepairDetailsModel _$RepairDetailsModelFromJson(Map<String, dynamic> json) {
  return RepairDetailsModel(
    json['result'] as String,
    json['msg'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RepairDetailsModelToJson(RepairDetailsModel instance) =>
    <String, dynamic>{
      'result': instance.result,
      'msg': instance.msg,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    json['equipmentCode'] as String,
    json['equipmentNameGroup'] as String,
    json['equipmentNameGroupId'] as String,
    json['exceptDescription'] as String,
    json['intervalTime'] as String,
    json['maintenOrgan'] as String,
    json['orderId'] as String,
    json['repairComplete'] as String,
    json['repairId'] as String,
    json['repairTime'] as String,
    json['reportPeople'] as String,
    json['reportPeopleID'] as String,
    json['reportTime'] as String,
    json['responsePeopleId'] as String,
    json['source'] as String,
    json['status'] as String,
    (json['process'] as List)
        ?.map((e) =>
            e == null ? null : Process.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['workReport'] == null
        ? null
        : WorkReport.fromJson(json['workReport'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'equipmentCode': instance.equipmentCode,
      'equipmentNameGroup': instance.equipmentNameGroup,
      'equipmentNameGroupId': instance.equipmentNameGroupId,
      'exceptDescription': instance.exceptDescription,
      'intervalTime': instance.intervalTime,
      'maintenOrgan': instance.maintenOrgan,
      'orderId': instance.orderId,
      'repairComplete': instance.repairComplete,
      'repairId': instance.repairId,
      'repairTime': instance.repairTime,
      'reportPeople': instance.reportPeople,
      'reportPeopleID': instance.reportPeopleID,
      'reportTime': instance.reportTime,
      'responsePeopleId': instance.responsePeopleId,
      'source': instance.source,
      'status': instance.status,
      'process': instance.process,
      'workReport': instance.workReport,
    };

Process _$ProcessFromJson(Map<String, dynamic> json) {
  return Process(
    json['createTime'] as String,
    json['operationLog'] as String,
  );
}

Map<String, dynamic> _$ProcessToJson(Process instance) => <String, dynamic>{
      'createTime': instance.createTime,
      'operationLog': instance.operationLog,
    };

WorkReport _$WorkReportFromJson(Map<String, dynamic> json) {
  return WorkReport(
    json['errorType'] as String,
    json['executedActivity'] as String,
    json['faultSymptoms'] as String,
    json['reason'] as String,
    json['reasonDetails'] as String,
    json['workDetails'] as String,
  );
}

Map<String, dynamic> _$WorkReportToJson(WorkReport instance) =>
    <String, dynamic>{
      'errorType': instance.errorType,
      'executedActivity': instance.executedActivity,
      'faultSymptoms': instance.faultSymptoms,
      'reason': instance.reason,
      'reasonDetails': instance.reasonDetails,
      'workDetails': instance.workDetails,
    };
