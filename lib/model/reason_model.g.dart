// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reason_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReasonModel _$ReasonModelFromJson(Map<String, dynamic> json) {
  return ReasonModel(
    json['TYPE'] as String,
    json['SUCCESSED'] as String,
    json['ERROR_MSG'] as String,
    (json['ERR_CAUSE_INFO'] as List)
        ?.map((e) => e == null
            ? null
            : ERR_CAUSE_INFO.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['SuccessFlag'] as String,
    json['ErrorMsg'] as String,
  );
}

Map<String, dynamic> _$ReasonModelToJson(ReasonModel instance) =>
    <String, dynamic>{
      'TYPE': instance.tYPE,
      'SUCCESSED': instance.sUCCESSED,
      'ERROR_MSG': instance.eRRORMSG,
      'ERR_CAUSE_INFO': instance.eRRCAUSEINFO,
      'SuccessFlag': instance.successFlag,
      'ErrorMsg': instance.errorMsg,
    };

ERR_CAUSE_INFO _$ERR_CAUSE_INFOFromJson(Map<String, dynamic> json) {
  return ERR_CAUSE_INFO(
    json['ERR_CAUSE'] as String,
    json['ERR_CAUSE_DESC'] as String,
  );
}

Map<String, dynamic> _$ERR_CAUSE_INFOToJson(ERR_CAUSE_INFO instance) =>
    <String, dynamic>{
      'ERR_CAUSE': instance.eRRCAUSE,
      'ERR_CAUSE_DESC': instance.eRRCAUSEDESC,
    };
