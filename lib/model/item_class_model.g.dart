// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_class_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemClassModel _$ItemClassModelFromJson(Map<String, dynamic> json) {
  return ItemClassModel(
    (json['PKG_INFO'] as List)
        ?.map((e) =>
            e == null ? null : PKG_INFO.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['CHK_INFO'] as List)
        ?.map((e) =>
            e == null ? null : CHK_INFO.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['LOGIC_EQUIPMENT_INFO'] as List)
        ?.map((e) => e == null
            ? null
            : LOGIC_EQUIPMENT_INFO.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['SuccessFlag'] as String,
    json['ErrorMsg'] as String,
  );
}

Map<String, dynamic> _$ItemClassModelToJson(ItemClassModel instance) =>
    <String, dynamic>{
      'PKG_INFO': instance.pKGINFO,
      'CHK_INFO': instance.cHKINFO,
      'LOGIC_EQUIPMENT_INFO': instance.lOGICEQUIPMENTINFO,
      'SuccessFlag': instance.successFlag,
      'ErrorMsg': instance.errorMsg,
    };

PKG_INFO _$PKG_INFOFromJson(Map<String, dynamic> json) {
  return PKG_INFO(
    json['EAM_TYPE'] as String,
    json['SUCCESSED'] as String,
    json['ERROR_MSG'] as String,
  );
}

Map<String, dynamic> _$PKG_INFOToJson(PKG_INFO instance) => <String, dynamic>{
      'EAM_TYPE': instance.eAMTYPE,
      'SUCCESSED': instance.sUCCESSED,
      'ERROR_MSG': instance.eRRORMSG,
    };

CHK_INFO _$CHK_INFOFromJson(Map<String, dynamic> json) {
  return CHK_INFO(
    json['TOTAL_LOGIC_EQUIPMENT'] as int,
  );
}

Map<String, dynamic> _$CHK_INFOToJson(CHK_INFO instance) => <String, dynamic>{
      'TOTAL_LOGIC_EQUIPMENT': instance.tOTALLOGICEQUIPMENT,
    };

LOGIC_EQUIPMENT_INFO _$LOGIC_EQUIPMENT_INFOFromJson(Map<String, dynamic> json) {
  return LOGIC_EQUIPMENT_INFO(
    json['MCH_CODE'] as String,
    json['MCH_NAME'] as String,
    json['CONTRACT'] as String,
    json['SUP_MCH_CODE'] as String,
    json['SUP_CONTRACT'] as String,
    json['OBJ_LEVEL'] as String,
    json['ITEM_CLASS_ID'] as String,
  );
}

Map<String, dynamic> _$LOGIC_EQUIPMENT_INFOToJson(
        LOGIC_EQUIPMENT_INFO instance) =>
    <String, dynamic>{
      'MCH_CODE': instance.mCHCODE,
      'MCH_NAME': instance.mCHNAME,
      'CONTRACT': instance.cONTRACT,
      'SUP_MCH_CODE': instance.sUPMCHCODE,
      'SUP_CONTRACT': instance.sUPCONTRACT,
      'OBJ_LEVEL': instance.oBJLEVEL,
      'ITEM_CLASS_ID': instance.iTEMCLASSID,
    };
