// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'executed_activities_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExecutedActivitiesModel _$ExecutedActivitiesModelFromJson(
    Map<String, dynamic> json) {
  return ExecutedActivitiesModel(
    json['TYPE'] as String,
    json['SUCCESSED'] as String,
    json['ERROR_MSG'] as String,
    (json['ACTION_INFO'] as List)
        ?.map((e) =>
            e == null ? null : ACTION_INFO.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['SuccessFlag'] as String,
    json['ErrorMsg'] as String,
  );
}

Map<String, dynamic> _$ExecutedActivitiesModelToJson(
        ExecutedActivitiesModel instance) =>
    <String, dynamic>{
      'TYPE': instance.tYPE,
      'SUCCESSED': instance.sUCCESSED,
      'ERROR_MSG': instance.eRRORMSG,
      'ACTION_INFO': instance.aCTIONINFO,
      'SuccessFlag': instance.successFlag,
      'ErrorMsg': instance.errorMsg,
    };

ACTION_INFO _$ACTION_INFOFromJson(Map<String, dynamic> json) {
  return ACTION_INFO(
    json['PERFORMED_ACTION_ID'] as String,
    json['DESCRIPTION'] as String,
  );
}

Map<String, dynamic> _$ACTION_INFOToJson(ACTION_INFO instance) =>
    <String, dynamic>{
      'PERFORMED_ACTION_ID': instance.pERFORMEDACTIONID,
      'DESCRIPTION': instance.dESCRIPTION,
    };
