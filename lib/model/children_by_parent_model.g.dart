// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'children_by_parent_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChildrenByParentModel _$ChildrenByParentModelFromJson(
    Map<String, dynamic> json) {
  return ChildrenByParentModel(
    json['result'] as String,
    json['msg'] as String,
    (json['data'] as List)
        ?.map(
            (e) => e == null ? null : Data.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ChildrenByParentModelToJson(
        ChildrenByParentModel instance) =>
    <String, dynamic>{
      'result': instance.result,
      'msg': instance.msg,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    json['MCH_CODE'] as String,
    json['MCH_NAME'] as String,
    json['CONTRACT'] as String,
    json['SUP_MCH_CODE'] as String,
    json['SUP_CONTRACT'] as String,
    json['OBJ_LEVEL'] as String,
    json['ITEM_CLASS_ID'] as String,
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'MCH_CODE': instance.mCHCODE,
      'MCH_NAME': instance.mCHNAME,
      'CONTRACT': instance.cONTRACT,
      'SUP_MCH_CODE': instance.sUPMCHCODE,
      'SUP_CONTRACT': instance.sUPCONTRACT,
      'OBJ_LEVEL': instance.oBJLEVEL,
      'ITEM_CLASS_ID': instance.iTEMCLASSID,
    };
