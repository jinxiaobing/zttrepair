// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'normal_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NormalModel _$NormalModelFromJson(Map<String, dynamic> json) {
  return NormalModel(
    json['result'] as String,
    json['msg'] as String,
  );
}

Map<String, dynamic> _$NormalModelToJson(NormalModel instance) =>
    <String, dynamic>{
      'result': instance.result,
      'msg': instance.msg,
    };
