// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repair_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RepairListModel _$RepairListModelFromJson(Map<String, dynamic> json) {
  return RepairListModel(
    json['RowNumber'] as String,
    json['repairId'] as String,
    json['reportPeopleID'] as String,
    json['reportPeople'] as String,
    json['reportTime'] as String,
    json['repairTime'] as String,
    json['repairComplete'] as String,
    json['intervalTime'] as String,
    json['engineers'] as String,
    json['engineerCode'] as String,
    json['newPart'] as String,
    json['orderId'] as String,
    json['equipmentCode'] as String,
    json['equipmentName'] as String,
    json['maintenOrgan'] as String,
    json['exceptDescription'] as String,
    json['equipmentNameGroup'] as String,
  );
}

Map<String, dynamic> _$RepairListModelToJson(RepairListModel instance) =>
    <String, dynamic>{
      'RowNumber': instance.rowNumber,
      'repairId': instance.repairId,
      'reportPeopleID': instance.reportPeopleID,
      'reportPeople': instance.reportPeople,
      'reportTime': instance.reportTime,
      'repairTime': instance.repairTime,
      'repairComplete': instance.repairComplete,
      'intervalTime': instance.intervalTime,
      'engineers': instance.engineers,
      'engineerCode': instance.engineerCode,
      'newPart': instance.newPart,
      'orderId': instance.orderId,
      'equipmentCode': instance.equipmentCode,
      'equipmentName': instance.equipmentName,
      'maintenOrgan': instance.maintenOrgan,
      'exceptDescription': instance.exceptDescription,
      'equipmentNameGroup': instance.equipmentNameGroup,
    };
