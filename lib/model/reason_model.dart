import 'package:json_annotation/json_annotation.dart';

part 'reason_model.g.dart';

@JsonSerializable()
class ReasonModel extends Object {
  @JsonKey(name: 'TYPE')
  String tYPE;

  @JsonKey(name: 'SUCCESSED')
  String sUCCESSED;

  @JsonKey(name: 'ERROR_MSG')
  String eRRORMSG;

  @JsonKey(name: 'ERR_CAUSE_INFO')
  List<ERR_CAUSE_INFO> eRRCAUSEINFO;

  @JsonKey(name: 'SuccessFlag')
  String successFlag;

  @JsonKey(name: 'ErrorMsg')
  String errorMsg;

  ReasonModel(
    this.tYPE,
    this.sUCCESSED,
    this.eRRORMSG,
    this.eRRCAUSEINFO,
    this.successFlag,
    this.errorMsg,
  );

  factory ReasonModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ReasonModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ReasonModelToJson(this);
}

@JsonSerializable()
class ERR_CAUSE_INFO extends Object {
  @JsonKey(name: 'ERR_CAUSE')
  String eRRCAUSE;

  @JsonKey(name: 'ERR_CAUSE_DESC')
  String eRRCAUSEDESC;

  ERR_CAUSE_INFO(
    this.eRRCAUSE,
    this.eRRCAUSEDESC,
  );

  factory ERR_CAUSE_INFO.fromJson(Map<String, dynamic> srcJson) =>
      _$ERR_CAUSE_INFOFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ERR_CAUSE_INFOToJson(this);
}
