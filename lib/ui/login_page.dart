import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:zttrepair/network/api_help.dart';
import 'package:zttrepair/utils/toast_utils.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  bool _isEditData = false;
  String _account = "";
  String _password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _buildTopWidget(),
              _buildBottomWidget(),
            ],
          ),
        ),
      ),
    );
  }

  ///创建上方widget
  _buildTopWidget() {
    return Container(
      color: color_default,
      height: 270,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Image.asset(
            "images/ic_login_logo.png",
            width: 83.5,
            height: 82.5,
          ),
          Positioned(
              bottom: 36,
              child: Text(
                "报修系统V2.0",
                style: TextStyle(
                  color: color_white,
                  fontSize: 18,
                ),
              ))
        ],
      ),
    );
  }

  ///创建下方widget
  _buildBottomWidget() {
    return Container(
      width: 228,
      margin: EdgeInsets.only(top: 45),
      child: Column(
        children: <Widget>[
          _bulidAccountTextField(),
          _bulidPasswordTextField(),
          Container(
            margin: EdgeInsets.only(top: 46),
            child: RaisedButton(
              onPressed: _isEditData ? _clickLogin : null,
              color: color_default,
              child: Text(
                "登录",
                style: TextStyle(color: color_white, fontSize: 16),
              ),
            ),
            width: 228,
            height: 40,
          ),
        ],
      ),
    );
  }

  void _textFieldChangedAccount(String account) {
    this._account = account;
    _checkData();
  }

  void _textFieldChangedPassword(String password) {
    this._password = password;
    _checkData();
  }

  ///点击登录
  _clickLogin() {
    ApiHelp.login(
        username: _account,
        password: _password,
        loginSuccess: () {
          Application.router.navigateTo(
              context, '${Routes.main}', clearStack: true);
        },
        loginError: (error) {
          ToastUtils.showShort(error.toString());
        });
  }

  ///检查用户名和密码
  void _checkData() {
    if (!TextUtil.isEmpty(this._account) && !TextUtil.isEmpty(this._password)) {
      _isEditData = true;
    } else {
      _isEditData = false;
    }
    setState(() {});
  }

  ///构建账户输入框
  _bulidAccountTextField() {
    return TextField(
      controller: _buildTextController(_account),
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: "账号",
        suffixIcon: TextUtil.isEmpty(_account)
            ? null
            : IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  _account = "";
                  _password = "";
                  _checkData();
                }),
      ),
      onChanged: _textFieldChangedAccount,
    );
  }

  ///构建密码输入框
  _bulidPasswordTextField() {
    return TextField(
      controller: _buildTextController(_password),
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.lock),
        labelText: "密码",
      ),
      onChanged: _textFieldChangedPassword,
    );
  }

  ///创建textController
  _buildTextController(String str) {
    return TextEditingController.fromValue(TextEditingValue(
        text: str,
        selection: TextSelection.fromPosition(TextPosition(
            affinity: TextAffinity.downstream, offset: str.length))));
  }
}
