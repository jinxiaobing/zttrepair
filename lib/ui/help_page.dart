import 'package:flutter/material.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HelpPage extends StatefulWidget {
  @override
  _HelpPageState createState() {
    return _HelpPageState();
  }
}

class _HelpPageState extends State<HelpPage> {
  static WebViewController webViewController;
  WebView _webView = WebView(
    initialUrl: "http://192.168.30.15:8080/NoticeHelp/index.html",
    onWebViewCreated: (WebViewController web) {
      webViewController = web;
      // webview 创建调用，
      web.loadUrl("http://192.168.30.15:8080/NoticeHelp/index.html");
      web.canGoBack().then((res) {
        print(res); // 是否能返回上一级
      });
      web.currentUrl().then((url) {
        print(url); // 返回当前url
      });
      web.canGoForward().then((res) {
        print(res); //是否能前进
      });
    },
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "消息推送帮助说明",
            style: TextStyle(color: color_white),
          ),
          iconTheme: IconThemeData(color: color_white),
          centerTitle: true,
        ),
        body: Stack(
          children: <Widget>[_webView],
        ),
      ),
      onWillPop: _clickBack,
    );
  }

  Future<bool> _clickBack() {
    webViewController.canGoBack().then((res) {
      if (res) {
        webViewController.goBack();
      } else {
        Navigator.pop(context);
      }
    });
  }
}
