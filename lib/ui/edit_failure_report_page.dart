import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:zttrepair/model/err_type_model.dart';
import 'package:zttrepair/model/executed_activities_model.dart';
import 'package:zttrepair/model/item_class_model.dart';
import 'package:zttrepair/model/reason_model.dart';
import 'package:zttrepair/network/api_help.dart';
import 'package:zttrepair/utils/toast_utils.dart';

class EditFailureReportPage extends StatefulWidget {
  String orderId;
  String repairId;
  String MCH_CODE;

  EditFailureReportPage(this.orderId, this.repairId, this.MCH_CODE);

  @override
  _EditFailureReportPageState createState() {
    return _EditFailureReportPageState(orderId, repairId, MCH_CODE);
  }
}

class _EditFailureReportPageState extends State<EditFailureReportPage> {
  String orderId;
  String repairId;
  String MCH_CODE;
  String executedActivity;
  String executedActivityId;
  String errType;
  String errTypeId;
  String reason;
  String reasonId;
  String ITEM_CLASS_ID;

  _EditFailureReportPageState(this.orderId, this.repairId, this.MCH_CODE);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "填写工单报告",
          style: TextStyle(color: color_white),
        ),
        iconTheme: IconThemeData(color: color_white),
      ),
      body: Column(
        children: <Widget>[
          _buildRow("工单号：", orderId, null),
          _buildLine(),
          _buildRow("已执行活动：", executedActivity, _clickActivity),
          _buildLine(),
          _buildRow("错误类型：", errType, _clickErrorType),
          _buildLine(),
          _buildRow("原因：", reason, _clickReason),
          _buildLine(),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Container(
            margin: EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width,
            child: RaisedButton(
              onPressed: _submit,
              color: color_default,
              child: Text(
                "提交",
                style: TextStyle(color: color_white, fontSize: 16),
              ),
            ),
            height: 40,
          ),
        ],
      ),
    );
  }

  _buildRow(String key, String value, Function function) {
    return GestureDetector(
      onTap: function,
      child: Container(
        padding: EdgeInsets.all(6),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(key),
            ),
            Text(
              TextUtil.isEmpty(value) ? "点击选择" : value,
              style: TextStyle(
                  color: TextUtil.isEmpty(value) ? Colors.grey : Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  _buildLine() {
    return Container(
      color: Colors.grey,
      width: MediaQuery.of(context).size.width,
      height: 1,
      margin: EdgeInsets.only(left: 6, right: 6),
    );
  }

  ///点击执行活动，请求ERP接口
  _clickActivity() {
    ApiHelp.chooseActivity(
        httpSuccess: (ExecutedActivitiesModel executedActivitiesModel) {
          if (executedActivitiesModel.aCTIONINFO == null ||
              executedActivitiesModel.aCTIONINFO.length == 0) {
            ToastUtils.showShort("无已执行活动选项");
            return;
          }
          List<String> items = List();
          for (int i = 0; i < executedActivitiesModel.aCTIONINFO.length; i++) {
            items.add(executedActivitiesModel.aCTIONINFO[i].dESCRIPTION);
          }
          _showPick(items, (Picker picker, List value) {
            executedActivity =
                executedActivitiesModel.aCTIONINFO[value[0]].dESCRIPTION;
            executedActivityId =
                executedActivitiesModel.aCTIONINFO[value[0]].pERFORMEDACTIONID;
            setState(() {});
          });
        },
        httpError: (error) => ToastUtils.showShort(error.toString()));
  }

  ///点击错误类型，先获取逻辑设备，再获取错误类型
  _clickErrorType() {
    ApiHelp.getItemClass(
        inAttr: "{\"MCH_CODE\":\"" + MCH_CODE + "\"}",
        httpSuccess: (ItemClassModel itemClassModel) {
          if (itemClassModel.lOGICEQUIPMENTINFO == null ||
              itemClassModel.lOGICEQUIPMENTINFO.length == 0) {
            ToastUtils.showShort("无项目类别");
            return;
          }
          ITEM_CLASS_ID = itemClassModel.lOGICEQUIPMENTINFO[0].iTEMCLASSID;
          _getErrorType(ITEM_CLASS_ID);
        },
        httpError: (error) => ToastUtils.showShort(error.toString()));
  }

  ///获取错误类型
  _getErrorType(String itemClassId) {
    ApiHelp.getErrorType(
        inAttr: "{\"ITEM_CLASS_ID\":\"" + itemClassId + "\"}",
        httpSuccess: (ErrTypeModel errTypeModel) {
          if (errTypeModel.eRRTYPEINFO == null ||
              errTypeModel.eRRTYPEINFO.length == 0) {
            ToastUtils.showShort("无错误类型选项");
            return;
          }
          List<String> items = List();
          for (int i = 0; i < errTypeModel.eRRTYPEINFO.length; i++) {
            items.add(errTypeModel.eRRTYPEINFO[i].eRRTYPEDESC);
          }
          _showPick(items, (Picker picker, List value) {
            this.errType = errTypeModel.eRRTYPEINFO[value[0]].eRRTYPEDESC;
            this.errTypeId = errTypeModel.eRRTYPEINFO[value[0]].eRRTYPE;
            setState(() {});
          });
        },
        httpError: (error) => ToastUtils.showShort(error.toString()));
  }

  _clickReason() {
    if (TextUtil.isEmpty(errTypeId)) {
      ToastUtils.showShort("请先选择错误类型");
      return;
    }
    ApiHelp.getReason(
        inAttr:
            "{\"ITEM_CLASS_ID\":\"$ITEM_CLASS_ID\",\"ERR_TYPE\":\"$errTypeId\"}",
        httpSuccess: (ReasonModel reasonModel) {
          if (reasonModel.eRRCAUSEINFO == null ||
              reasonModel.eRRCAUSEINFO.length == 0) {
            ToastUtils.showShort("无原因选项");
            return;
          }
          List<String> items = List();
          for (int i = 0; i < reasonModel.eRRCAUSEINFO.length; i++) {
            items.add(reasonModel.eRRCAUSEINFO[i].eRRCAUSEDESC);
          }
          _showPick(items, (Picker picker, List value) {
            this.reason = reasonModel.eRRCAUSEINFO[value[0]].eRRCAUSEDESC;
            this.reasonId = reasonModel.eRRCAUSEINFO[value[0]].eRRCAUSE;
            setState(() {});
          });
        },
        httpError: (error) => ToastUtils.showShort(error.toString()));
  }

  ///显示pickview
  void _showPick(List<String> items, Function function) {
    Picker(
            adapter: PickerDataAdapter<String>(pickerdata: items),
            changeToFirst: true,
            hideHeader: false,
            confirmText: "确定",
            confirmTextStyle: TextStyle(color: color_white, fontSize: 16),
            cancelTextStyle: TextStyle(color: color_white, fontSize: 16),
            cancelText: "取消",
            headercolor: color_default,
            height: 150,
            textAlign: TextAlign.left,
            textStyle: const TextStyle(color: Colors.black, fontSize: 16),
            selectedTextStyle: TextStyle(color: color_default, fontSize: 16),
            onConfirm: function)
        .showModal(this.context);
  }

  _submit() {
    if (!checkDataset()) {
      return;
    }
    String inAttr =
        "{\"WO_NO\":\"$orderId\",\"ITEM_CLASS_ID\":\"$ITEM_CLASS_ID\",\"PERFORMED_ACTION_ID\":\"$executedActivityId\",\"ERR_TYPE\":\"$errTypeId\",\"ERR_CAUSE\":\"$reasonId\",\"MCH_CODE\":\"$MCH_CODE\"}";
    ApiHelp.modifyWoreport(
        inAttr: inAttr,
        errorType: errType,
        executedActivity: executedActivity,
        reason: reason,
        repairId: repairId,
        WO_NO: orderId,
        httpSuccess: () {
          ToastUtils.showShort("操作成功");
          Navigator.pop(context);
        },
        httpError: (error) => ToastUtils.showShort(error.toString()));
  }

  bool checkDataset() {
    if (TextUtil.isEmpty(executedActivityId)) {
      ToastUtils.showShort("请选择已执行活动");
      return false;
    }
    if (TextUtil.isEmpty(errTypeId)) {
      ToastUtils.showShort("请选择错误类型");
      return false;
    }
    if (TextUtil.isEmpty(reasonId)) {
      ToastUtils.showShort("请选择原因");
      return false;
    }
    return true;
  }
}
