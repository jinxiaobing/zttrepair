import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:flustars/flustars.dart';
import 'package:zttrepair/config/sp_tag.dart';
import 'package:zttrepair/push/push_manager.dart';
import 'package:zttrepair/utils/dialog_utils.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {
  List<String> _modelNames;
  List<String> _modelIcons;

  @override
  void initState() {
    super.initState();
    _modelNames = ["待响应", "待开始", "待完成", "待报告", "待关闭", "已关闭"];
    _modelIcons = [
      "images/icon_dxy.png",
      "images/icon_dks.png",
      "images/icon_dwc.png",
      "images/icon_dbg.png",
      "images/icon_dgb.png",
      "images/icon_ygb.png"
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  _buildAppBar() {
    return AppBar(
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.help, color: color_white),
        onPressed: () =>
            Application.router.navigateTo(context, '${Routes.help}'),
      ),
      title: Text(
        "报修系统V2.0",
        style: TextStyle(color: color_white),
      ),
      actions: <Widget>[
        Center(
            child: Padding(
              padding: EdgeInsets.only(right: 15),
              child: FlatButton(
                onPressed: () =>
                    DialogUtils.showConfirmDialog(
                      context,
                      "确定注销登录吗？",
                      _logout,
                    ),
                color: Colors.transparent,
                child: Text(
                  "注销账号",
                  style: TextStyle(color: color_white),
                ),
              ),
            ))
      ],
    );
  }

  _buildBody() {
    return Column(
      children: <Widget>[
        _buildUserInfo(),
        _buildModels(),
      ],
    );
  }

  _buildUserInfo() {
    return Container(
      color: Color(0xfff5f8f9),
      height: 183,
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 33, left: 33),
            width: 150,
            height: 150,
            child: Image.asset(
              "images/ic_main_user.png",
              fit: BoxFit.contain,
            ),
          ),
          Center(
            child: Container(
              height: 183,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(),
                      flex: 1,
                    ),
                    Text(SpUtil.getString(SP_TAG_REALNAME) + "工程师"),
                    Text("欢迎回来"),
                    Text(DateUtil.formatDateMs(
                        DateTime
                            .now()
                            .millisecondsSinceEpoch,
                        format: DataFormats.y_mo_d)),
                    Text(DateUtil.formatDateMs(
                        DateTime
                            .now()
                            .millisecondsSinceEpoch,
                        format: DataFormats.h_m_s)),
                    Expanded(
                      child: Container(),
                      flex: 1,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildModels() {
    return Expanded(
      child: GridView.count(
        //水平子Widget之间间距
        crossAxisSpacing: 30.0,
        //垂直子Widget之间间距
        mainAxisSpacing: 5.0,
        //GridView内边距
        padding: EdgeInsets.all(10.0),
        //一行的Widget数量
        crossAxisCount: 3,
        //子Widget宽高比例
        childAspectRatio: 1.0,
        //子Widget列表
        children: _getModelList(),
      ),
    );
  }

  List<GestureDetector> _getModelList() {
    return List<GestureDetector>.generate(
        6,
            (int index) =>
            GestureDetector(
              onTap: () {
                _clickModel(index);
              },
              child: Container(
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      _modelIcons[index],
                      width: 60,
                      height: 60,
                    ),
                    Expanded(
                      child: Center(
                        child: Text(_modelNames[index]),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }

  _clickModel(int index) {
    String title = jsonEncode(Utf8Encoder().convert(_modelNames[index]));
    String tag = index.toString();
    String path = '${Routes.repair_list}?title=$title&tag=$tag';
    Application.router.navigateTo(context, path);
  }

  Function _logout() {
    SpUtil.clear();
    PushManager.getInstance().deleteAlias();
    Application.router.navigateTo(context, '${Routes.login}', clearStack: true);
  }
}
