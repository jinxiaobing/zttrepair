import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:zttrepair/config/sp_tag.dart';
import 'package:zttrepair/model/repair_list_model.dart';
import 'package:zttrepair/network/api_help.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flustars/flustars.dart';

class RepairListPage extends StatefulWidget {
  String title;
  String tag;

  @override
  _RepairListPageState createState() {
    return _RepairListPageState(title, tag);
  }

  RepairListPage({@required String title, @required String tag}) {
    this.title = title;
    this.tag = tag;
  }
}

class _RepairListPageState extends State<RepairListPage>
    with WidgetsBindingObserver {
  String title;
  String tag;
  List<RepairListModel> items = [];
  RefreshController _refreshController =
  RefreshController(initialRefresh: true);
  _RepairListPageState(String title, String tag) {
    this.tag = tag;
    var list = List<int>();
    jsonDecode(title).forEach(list.add);
    this.title = Utf8Decoder().convert(list);
  }
  int pageSize = 5;
  int pageNum = 1;
  bool _enablePullDown = true;
  bool _enablePullUp = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this); //添加观察者
  }

  ///生命周期变化时回调
//  resumed:应用可见并可响应用户操作
//  inactive:用户可见，但不可响应用户操作
//  paused:已经暂停了，用户不可见、不可操作
//  suspending：应用被挂起，此状态IOS永远不会回调
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      setState(() {
        _onRefresh();
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this); //销毁观察者
    _refreshController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(color: color_white),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: color_white),
      ),
      body: SmartRefresher(
        controller: _refreshController,
        enablePullDown: _enablePullDown,
        enablePullUp: _enablePullUp,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          itemBuilder: (c, i) => _bulidListItem(c, i),
//          itemExtent: 100.0,
          itemCount: items.length,
        ),
      ),
    );
  }

  void _onRefresh() async {
    pageNum = 1;
    ApiHelp.getRepairList(
        username: SpUtil.getString(SP_TAG_USERNAME),
        tag: tag,
        pageSize: pageSize.toString(),
        pageNum: pageNum.toString(),
        getRepairListSuccess: _onRefreshSuccess,
        getRepairListError: _onRefreshError);
  }

  void _onRefreshSuccess(List<RepairListModel> repairModels) {
    items.clear();
    items.addAll(repairModels);
    _refreshController.refreshCompleted();
    _enablePullUp = repairModels.length >= pageSize;
    setState(() {});
  }

  void _onRefreshError() {
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    pageNum++;
    ApiHelp.getRepairList(
        username: SpUtil.getString(SP_TAG_USERNAME),
        tag: tag,
        pageSize: pageSize.toString(),
        pageNum: pageNum.toString(),
        getRepairListSuccess: _onLoadingSuccess,
        getRepairListError: _onLoadingError);
  }

  void _onLoadingSuccess(List<RepairListModel> repairModels) {
    items.addAll(repairModels);
    _refreshController.loadComplete();
    _enablePullUp = repairModels.length >= pageSize;
    setState(() {});
  }

  void _onLoadingError() {
    _refreshController.loadComplete();
  }



  String lastHeaer = "";

  ///构造列表的item
  _bulidListItem(BuildContext c, int i) {
    String no = (i + 1).toString();
    String reportTime = items[i].reportTime;
    String maintenOrgan = items[i].maintenOrgan;
    String exceptDescription = items[i].exceptDescription;
    String equipmentCode = items[i].equipmentCode;
    String orderId = items[i].orderId;
    String equipmentNameGroup = items[i].equipmentNameGroup;
    List<String> groups = equipmentNameGroup.split("\|");
    String equipmengName = "";
    for (int i = 0; i < groups.length; i++) {
      if (i < 3) continue;
      equipmengName += groups[i];
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _getHead(reportTime),
        GestureDetector(
          onTap: () {
            _onItemClick(i);
          },
          child: Container(
            margin: EdgeInsets.only(left: 6),
            color: color_default,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 6),
                      width: 27,
                      child: Center(
                        child: Text(
                          no,
                          style: TextStyle(color: color_white),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        color: color_white,
                        padding: EdgeInsets.only(right: 30),
                        child: Column(
                          children: <Widget>[
                            _bulidItemContent("工单号：", orderId),
                            _bulidItemContent("设备编号：", equipmentCode),
                            _bulidItemContent("设备描述：", equipmengName),
                            _bulidItemContent("维护组织：", maintenOrgan),
                            _bulidItemContent("故障详情：", exceptDescription),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  child: Text(reportTime),
                  right: 5,
                  top: 0,
                ),
                Positioned(
                  right: 5,
                  child: Icon(
                    Icons.keyboard_arrow_right,
                    color: color_default,
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  ///构造列表item中的内容项
  _bulidItemContent(String title, String content) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(color: Colors.black),
        ),
        Expanded(
          child: Text(
            content,
            style: TextStyle(color: Colors.black),
          ),
        ),
      ],
    );
  }

  _getHead(String time) {
    time = time.replaceAll("/0", "/").split(" ")[0];
    String today = DateUtil.formatDateMs(DateTime
        .now()
        .millisecondsSinceEpoch,
        format: "yyyy/M/d");
    String yesterday = DateUtil.formatDateMs(
        DateTime
            .now()
            .millisecondsSinceEpoch - 24 * 60 * 60 * 1000,
        format: "yyyy/M/d");
    String head = "";
    if (time == today) {
      head = time == lastHeaer ? "" : "今天";
      lastHeaer = "今天";
    } else if (time == yesterday && lastHeaer == yesterday) {
      head = time == lastHeaer ? "" : "昨天";
      lastHeaer = "昨天";
    } else {
      head = time == lastHeaer ? "" : time;
      lastHeaer = time;
    }
    if (head == "") {
      return Container(
        margin: EdgeInsets.only(top: 2),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(left: 6, top: 6, bottom: 6),
        child: Text(head),
      );
    }
  }

  ///列表点击事件
  _onItemClick(int i) {
    String repairId = items[i].repairId;
    String path = '${Routes.repair_details}?repairId=$repairId';
    Application.router.navigateTo(context, path).then((s) {
      setState(() {
        _onRefresh();
      });
    });
  }
}
