import 'package:flutter/material.dart';
import 'package:flustars/flustars.dart';
import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';
import 'package:zttrepair/network/api_help.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() {
    return _WelcomePageState();
  }
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(new Duration(seconds: 3), () {
      String username = SpUtil.getString("username");
      String password = SpUtil.getString("password");
      if (TextUtil.isEmpty(username) || TextUtil.isEmpty(password)) {
        _toLoginPage();
      } else {
        _autoLogin(username, password);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Hero(
          tag: "page",
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Image.asset(
              "images/bg_welcome.jpg",
              fit: BoxFit.cover,
            ),
          )),
    );
  }

  void _toLoginPage() {
    Application.router.navigateTo(context, '${Routes.login}', clearStack: true);
  }

  void _autoLogin(String username, String password) {
    ApiHelp.login(
        username: username,
        password: password,
        loginSuccess: () {
          Application.router
              .navigateTo(context, '${Routes.main}', clearStack: true);
        },
        loginError: (error) {
          _toLoginPage();
        });
  }
}
