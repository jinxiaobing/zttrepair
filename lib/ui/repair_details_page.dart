import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:zttrepair/config/sp_tag.dart';
import 'package:zttrepair/model/repair_details_model.dart';
import 'package:zttrepair/network/api_help.dart';
import 'package:zttrepair/utils/toast_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flustars/flustars.dart';

class RepairDetailsPage extends StatefulWidget {
  String repairId;

  @override
  _RepairDetailsPageState createState() {
    return _RepairDetailsPageState(repairId);
  }

  RepairDetailsPage({@required String repairId}) {
    this.repairId = repairId;
  }
}

class _RepairDetailsPageState extends State<RepairDetailsPage> {
  String repairId;
  RepairDetailsModel _repairDetailsModel;
  String equipmengName = "";
  String _status;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<String> response_time = ["5分钟", "10分钟", "15分钟", "30分钟", "45分钟", "60分钟"];
  List<String> response_time_str = ["5", "10", "15", "30", "45", "60"];

  _RepairDetailsPageState(String repairId) {
    this.repairId = repairId;
  }

  @override
  void initState() {
    super.initState();
    _getRepairDetais(repairId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(
            "报修详情",
            style: TextStyle(color: color_white),
          ),
          centerTitle: true,
          iconTheme: IconThemeData(color: color_white),
        ),
        body: _buildBody());
  }

  void _getRepairDetais(String repairId) {
    ApiHelp.getRepairDetails(
        repairId: repairId,
        getRepairDetailsSuccess: _getRepairDetailsSuccess,
        getRepairDetailsError: _getRepairDetailsError);
  }

  _getRepairDetailsSuccess(RepairDetailsModel repairDetailsModel) {
    this._repairDetailsModel = repairDetailsModel;
    String equipmentNameGroup = _repairDetailsModel.data.equipmentNameGroup;
    List<String> groups = equipmentNameGroup.split("\|");
    equipmengName = "";
    for (int i = 0; i < groups.length; i++) {
      if (i < 3) continue;
      equipmengName += groups[i];
    }
    _status = repairDetailsModel.data.status;
    setState(() {});
  }

  _getRepairDetailsError(String error) {
    Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER);
  }

  _bulidInfo(String key, String value) {
    if ((value == null || value == "") && key != "流程状态：") {
      return Container();
    }
    if (key == "故障报告：" && _repairDetailsModel.data.workReport == null) {
      return Container();
    }
    if (key == "维修时长：") {
      value += "小时";
    }
    return Container(
      padding: EdgeInsets.all(6),
      child: Row(
        children: <Widget>[
          Expanded(child: Text(key)),
          Text(value),
        ],
      ),
    );
  }

  ///构建主体
  _buildBody() {
    if (_repairDetailsModel == null) {
      return Container();
    }
    return Container(
      height: MediaQuery
          .of(context)
          .size
          .height,
      child: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  _bulidInfo("工单号：", _repairDetailsModel.data.orderId),
                  _bulidInfo("设备编号：", _repairDetailsModel.data.equipmentCode),
                  _bulidInfo("设备描述：", equipmengName),
                  _bulidInfo("维护组织：", _repairDetailsModel.data.maintenOrgan),
                  _bulidInfo(
                      "故障详情：", _repairDetailsModel.data.exceptDescription),
                  _bulidInfo("报修人员：", _repairDetailsModel.data.reportPeople),
                  _bulidInfo("报修时间：", _repairDetailsModel.data.reportTime),
                  _bulidInfo(
                      "维修状态：",
                      _status == "0"
                          ? "待响应"
                          : _status == "1"
                          ? "已响应"
                          : _status == "2"
                          ? "维修中"
                          : _status == "3"
                          ? "工作完成"
                          : _status == "4" ? "已报告" : "已关闭"),
                  _bulidInfo("开始时间：", _repairDetailsModel.data.repairTime),
                  _bulidInfo("完成时间：", _repairDetailsModel.data.repairComplete),
                  _bulidInfo("维修时长：", _repairDetailsModel.data.intervalTime),
                  _bulidInfo("故障报告：", "点击查看"),
                  _bulidInfo("流程状态：", ""),
                  _buildList(_repairDetailsModel.data.process),
                ],
              ),
            ),
          ),
          _buildButtons(),
        ],
      ),
    );
  }

  ///构建流程状态列表
  _buildList(List<Process> process) {
    List<Widget> widget = [];
    for (int i = process.length - 1; i >= 0; i--) {
      widget.add(_bulidListItem(i, process));
    }
    return Column(
      children: widget,
    );
  }

  _bulidListItem(int i, List<Process> process) {
    String date = process[i].createTime.split(" ")[0];
    String time = process[i].createTime.split(" ")[1];
    return Container(
      height: 44,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Text(date),
                Text(time),
              ],
            ),
          ),
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Image.asset(
                "images/ic_details_progress_vertical.png",
              ),
              Image.asset(
                i == process.length - 1
                    ? "images/ic_details_progress_circle_red.png"
                    : "images/ic_details_progress_circle_white.png",
                width: 10,
                height: 10,
              ),
            ],
          ),
          Expanded(
            flex: 2,
            child: Container(
              child: Text(process[i].operationLog),
              padding: EdgeInsets.only(left: 6),
            ),
          ),
        ],
      ),
    );
  }

  ///构建按钮组
  _buildButtons() {
    switch (_status) {
      case "0":
        return _buildButton("响应", _showTimePick);
      case "3":
        return _buildChangeAndReportButtons();
      case "4":
        return _buildButton("关闭工单", _closeRepair);
      default:
        return Container();
    }
  }

  ///构建单个按钮
  _buildButton(String title, Function function) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
      child: RaisedButton(
        onPressed: function,
        child: Text(
          title,
          style: TextStyle(color: color_white),
        ),
        color: color_default,
      ),
    );
  }

  ///构建更换设备、填写报告按钮
  _buildChangeAndReportButtons() {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
      child: Row(
        children: <Widget>[
          Expanded(
            child: RaisedButton(
              onPressed: _toChangeEquipmentPage,
              child: Text(
                "修改设备",
                style: TextStyle(color: color_white),
              ),
              color: color_default,
            ),
            flex: 1,
          ),
          Container(
            width: 30,
          ),
          Expanded(
            child: RaisedButton(
              onPressed: _toEditReportPage,
              child: Text(
                "填写报告",
                style: TextStyle(color: color_white),
              ),
              color: color_default,
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  ///点击响应后，显示选择时间pick
  _showTimePick() {
    Picker(
        adapter: PickerDataAdapter<String>(pickerdata: response_time),
        changeToFirst: true,
        hideHeader: false,
        confirmText: "确定",
        confirmTextStyle: TextStyle(color: color_white, fontSize: 16),
        cancelTextStyle: TextStyle(color: color_white, fontSize: 16),
        cancelText: "取消",
        headercolor: color_default,
        height: 150,
        title: Text(
          "请选择多久后到达现场",
          style: TextStyle(color: color_white, fontSize: 16),
        ),
        textAlign: TextAlign.left,
        textStyle: const TextStyle(color: Colors.black, fontSize: 16),
        selectedTextStyle: TextStyle(color: color_default, fontSize: 16),
        onConfirm: (Picker picker, List value) {
          _response(response_time_str[value[0]]);
        }).showModal(this.context); //_scaffoldKey.currentState);
  }

  ///跳转填写报告页面
  void _toEditReportPage() {
    String orderId = _repairDetailsModel.data.orderId;
    String equipmentNameGroupId = _repairDetailsModel.data.equipmentNameGroupId;
    List equipmentNameGroupIds = equipmentNameGroupId.split("\|");
    String MCH_CODE = equipmentNameGroupIds[6];
    String path = '${Routes.edit_failure_report}?repairId=$repairId'
        '&MCH_CODE=$MCH_CODE'
        '&orderId=$orderId';
    Application.router.navigateTo(context, path).then((result) {
      _getRepairDetais(repairId);
    });
  }

  ///跳转修改设备页面
  void _toChangeEquipmentPage() {
    String orderId = _repairDetailsModel.data.orderId;
    String equipmentNameGroup = jsonEncode(
        Utf8Encoder().convert(_repairDetailsModel.data.equipmentNameGroup));
    String equipmentNameGroupId = _repairDetailsModel.data.equipmentNameGroupId;
    String path = '${Routes.change_equipment}?repairId=$repairId'
        '&equipmentNameGroup=$equipmentNameGroup'
        '&equipmentNameGroupId=$equipmentNameGroupId'
        '&orderId=$orderId';
    Application.router.navigateTo(context, path).then((result) {
      _getRepairDetais(repairId);
    });
  }

  ///关闭工单
  _closeRepair() {
    String orderId = _repairDetailsModel.data.orderId;
    String inAttr = "{\"WO_NO\":\"$orderId\",\"NEW_STATE\":\"FINISHED\"}";
    ApiHelp.closeWorkorder(
      inAttr: inAttr,
      repairId: repairId,
      httpSuccess: () {
        ToastUtils.showShort("关闭成功");
        _getRepairDetais(repairId);
      },
      httpError: (error) {
        ToastUtils.showShort(error.toString());
      },
    );
  }

  ///响应
  void _response(String responseTime) {
    ApiHelp.responseTime(
        username: SpUtil.getString(SP_TAG_USERNAME),
        isWait: "1",
        repairId: repairId,
        ResponseTimeLeft: responseTime,
        responseTimeSuccess: () {
          ToastUtils.showShort("响应成功");
          _getRepairDetais(repairId);
        },
        responseTimeError: (error) {
          ToastUtils.showShort(error.toString());
        });
  }
}
