import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:flustars/flustars.dart';
import 'package:zttrepair/config/sp_tag.dart';
import 'package:zttrepair/model/children_by_parent_model.dart';
import 'package:zttrepair/network/api_help.dart';
import 'package:zttrepair/utils/toast_utils.dart';

class ChangeEquipmentPage extends StatefulWidget {
  String equipmentNameGroup;
  String equipmentNameGroupId;
  String repairId;
  String orderId;

  @override
  _ChangeEquipmentPageState createState() {
    return _ChangeEquipmentPageState(
        equipmentNameGroup, equipmentNameGroupId, repairId, orderId);
  }

  ChangeEquipmentPage({
    @required this.equipmentNameGroup,
    @required this.equipmentNameGroupId,
    @required this.repairId,
    @required this.orderId,
  });
}

class _ChangeEquipmentPageState extends State<ChangeEquipmentPage> {
  String equipmentNameGroup;
  String equipmentNameGroupId;
  String repairId;
  String orderId;
  List<String> equipmentNameGroups;
  List<String> equipmentNameGroupIds;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  _ChangeEquipmentPageState(this.equipmentNameGroup, this.equipmentNameGroupId,
      this.repairId, this.orderId);

  @override
  void initState() {
    super.initState();
    var list = List<int>();
    jsonDecode(equipmentNameGroup).forEach(list.add);
    this.equipmentNameGroup = Utf8Decoder().convert(list);
    equipmentNameGroups = equipmentNameGroup.split("\|");
    equipmentNameGroupIds = equipmentNameGroupId.split("\|");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "修改设备信息",
          style: TextStyle(color: color_white),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: color_white),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildText(0),
          _buildLine(),
          _buildText(1),
          _buildLine(),
          _buildText(2),
          _buildLine(),
          _buildClickableText(3),
          _buildLine(),
          _buildClickableText(4),
          _buildLine(),
          _buildClickableText(5),
          _buildLine(),
          _buildClickableText(6),
          _buildLine(),
          _buildButton(),
        ],
      ),
    );
  }

  _buildText(int i) {
    String contentNull;
    switch (i) {
      case 3:
        contentNull = "点击选择系统";
        break;
      case 4:
        contentNull = "点击选择车间";
        break;
      case 5:
        contentNull = "点击选择工序";
        break;
      case 6:
        contentNull = "点击选择设备";
        break;
    }
    Text text;
    if (TextUtil.isEmpty(equipmentNameGroups[i])) {
      text = Text(
        contentNull,
        style: TextStyle(color: Colors.grey),
      );
    } else {
      text = Text(
        equipmentNameGroups[i],
        style: TextStyle(color: Colors.black),
      );
    }
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
      child: text,
    );
  }

  _buildClickableText(int i) {
    return GestureDetector(
      child: _buildText(i),
      onTap: () {
        _getChildrenByParent(i);
      },
    );
  }

  _buildLine() {
    return Container(
      color: Colors.grey,
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: 1,
      margin: EdgeInsets.only(left: 6, right: 6),
    );
  }

  _buildButton() {
    return Container(
      margin: EdgeInsets.all(15),
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: RaisedButton(
        onPressed: _submit,
        color: color_default,
        child: Text(
          "提交",
          style: TextStyle(color: color_white, fontSize: 16),
        ),
      ),
      height: 40,
    );
  }

  _getChildrenByParent(int index) {
    int parentIndex = index - 1;
    if (TextUtil.isEmpty(equipmentNameGroupIds[parentIndex])) {
      ToastUtils.showShort("请先选择上一级信息");
      return;
    }
    String parentPath = "";
    for (int i = 0; i < index; i++) {
      parentPath += equipmentNameGroupIds[i];
      if (i < index - 1) {
        parentPath += "|";
      }
    }
    ApiHelp.getChildrenByParent(
        username: SpUtil.getString(SP_TAG_USERNAME),
        parent: equipmentNameGroupIds[parentIndex],
        parentPath: parentPath,
        site: SpUtil.getString(SP_TAG_SITE),
        getChildrenByParentSuccess: (list) {
          _getChildrenByParentSuccess(index, list);
        },
        getChildrenByParentError: (error) {
          ToastUtils.showShort(error.toString());
        });
  }

  _getChildrenByParentSuccess(int index, List<Data> data) {
    if (data == null || data.length == 0) {
      ToastUtils.showShort("暂无选项");
      return;
    }
    final List<String> items = List();
    for (Data dataBean in data) {
      items.add(dataBean.mCHNAME);
    }
    Picker(
        adapter: PickerDataAdapter<String>(pickerdata: items),
        changeToFirst: true,
        hideHeader: false,
        confirmText: "确定",
        confirmTextStyle: TextStyle(color: color_white, fontSize: 16),
        cancelTextStyle: TextStyle(color: color_white, fontSize: 16),
        cancelText: "取消",
        headercolor: color_default,
        height: 150,
        textAlign: TextAlign.left,
        textStyle: const TextStyle(color: Colors.black, fontSize: 16),
        selectedTextStyle: TextStyle(color: color_default, fontSize: 16),
        onConfirm: (Picker picker, List value) {
          equipmentNameGroups[index] = data[value[0]].mCHNAME;
          equipmentNameGroupIds[index] = data[value[0]].mCHCODE;
          for (int i = index + 1; i < 7; i++) {
            equipmentNameGroups[i] = "";
            equipmentNameGroupIds[i] = "";
          }
          setState(() {});
        }).showModal(this.context);
  }

  void _submit() {
    if (checkDatas()) {
      String EquipmentNameGroup = "";
      String EquipmentNameGroupId = "";
      for (int i = 0; i < this.equipmentNameGroups.length; i++) {
        EquipmentNameGroup += equipmentNameGroups[i] + "|";
        EquipmentNameGroupId += equipmentNameGroupIds[i] + "|";
      }
      EquipmentNameGroup =
          EquipmentNameGroup.substring(0, EquipmentNameGroup.length - 1);
      EquipmentNameGroupId =
          EquipmentNameGroupId.substring(0, EquipmentNameGroupId.length - 1);
      ApiHelp.changeEquipmentInfo(
          repairId: repairId,
          EquipmentNameGroup: EquipmentNameGroup,
          EquipmentCodeGroup: EquipmentNameGroupId,
          equipmentName: equipmentNameGroups[equipmentNameGroups.length - 1],
          equipmentCode:
          equipmentNameGroupIds[equipmentNameGroupIds.length - 1],
          inAttr: "{\"WO_NO\":\"" +
              orderId +
              "\",\"MCH_CODE\":\"" +
              equipmentNameGroupIds[equipmentNameGroupIds.length - 1] +
              "\"}",
          httpSuccess: () {
            ToastUtils.showShort("修改成功");
            Navigator.pop(context);
          },
          httpError: (error) {
            ToastUtils.showShort(error.toString());
          });
    }
  }

  bool checkDatas() {
    if (TextUtil.isEmpty(equipmentNameGroupIds[3]) ||
        TextUtil.isEmpty(equipmentNameGroupIds[4]) ||
        TextUtil.isEmpty(equipmentNameGroupIds[5]) ||
        TextUtil.isEmpty(equipmentNameGroupIds[6])) {
      ToastUtils.showShort("请选择设备相关信息");
      return false;
    }
    return true;
  }
}
