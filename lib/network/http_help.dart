import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum PARSE_TYPE { JSON_OBJ, JSON_ARRAY }

class HttpHelp {
  static HttpHelp _httpHelp;
  static final String GET = "get";
  static final String POST = "post";
  static final String DATA = "data";
  static final String CODE = "errorCode";

//  static final String JSON_OBJ = "jsonObj";
//  static final String JSON_ARRAY = "jsonArray";

  Dio dio;

  static HttpHelp getInstance() {
    if (_httpHelp == null) {
      _httpHelp = HttpHelp();
    }
    return _httpHelp;
  }

  HttpHelp() {
    //初始化网络请求相关信息
    dio = Dio();
    dio.options.connectTimeout = 30000;
    dio.options.receiveTimeout = 30000;
  }

//get请求
  get(PARSE_TYPE parse_type, String url, Function successCallBack,
      {params, Function errorCallBack}) async {
    _requstHttp(parse_type, url, successCallBack, GET, params, errorCallBack);
  }

  //post请求
  post(PARSE_TYPE parse_type, String url, Function successCallBack,
      {params, Function errorCallBack}) async {
    _requstHttp(parse_type, url, successCallBack, POST, params, errorCallBack);
  }

  _requstHttp(PARSE_TYPE parse_type, String url, Function successCallBack,
      [String method, FormData params, Function errorCallBack]) async {
    String errorMsg = '';
    int code;

    try {
      Response response;
      if (method == GET) {
        if (params != null && params.isNotEmpty) {
          response = await dio.get(url, queryParameters: params);
        } else {
          response = await dio.get(url);
        }
      } else if (method == POST) {
        if (params != null && params.isNotEmpty) {
          response = await dio.post(url, queryParameters: params);
        } else {
          response = await dio.post(url);
        }
      }

      code = response.statusCode;
      if (code != 200) {
        errorMsg = '错误码：' + code.toString() + '，' + response.data.toString();
        _error(errorCallBack, errorMsg);
        return;
      }
      Object data;
      if (parse_type == PARSE_TYPE.JSON_OBJ) {
        data = json.decode(response.data) as Map<String, dynamic>;
      } else {
        data = json.decode(response.data) as List<dynamic>;
      }
      if (data != null && successCallBack != null) {
        successCallBack(data);
      }
    } catch (exception) {
      _error(errorCallBack, exception.toString());
    }
  }

  _error(Function errorCallBack, String error) {
    Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER);
    if (errorCallBack != null) {
      errorCallBack(error);
    }
  }
}
