import 'package:dio/dio.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:zttrepair/config/sp_tag.dart';
import 'package:zttrepair/model/children_by_parent_model.dart';
import 'package:zttrepair/model/err_type_model.dart';
import 'package:zttrepair/model/executed_activities_model.dart';
import 'package:zttrepair/model/item_class_model.dart';
import 'package:zttrepair/model/login_model.dart';
import 'package:zttrepair/model/normal_model.dart';
import 'package:zttrepair/model/reason_model.dart';
import 'package:zttrepair/model/repair_details_model.dart';
import 'package:zttrepair/model/repair_list_model.dart';
import 'package:zttrepair/network/http_help.dart';
import 'package:zttrepair/push/push_manager.dart';

class ApiHelp {
  static const RESULT_OK = "1";
  static const bool isRelease = false;

  ///MES基础接口
  static const String URL_TEST =
      "http://10.18.24.17:8015/Handler/ZTT_EquipmentRepair/";
  static const String URL_RELEASE =
      "http://ers.zttit.com:7272/Handler/ZTT_EquipmentRepair/";
  static const String BASE_URL = isRelease ? URL_RELEASE : URL_TEST;

  ///ERP基础接口
  static const String URL_ERP_TEST = "http://192.168.20.47/";
  static const String URL_ERP_RELEASE = "";
  static const String BASE_ERP_URL = isRelease ? URL_ERP_RELEASE : URL_ERP_TEST;

  static const String URL_LOGIN = BASE_URL + "Login.ashx";
  static const String URL_REPAIR_LIST = BASE_URL + "ProcessList.ashx";
  static const String URL_REPAIR_DETAILS = BASE_URL + "getRepairDetails.ashx";
  static const String URL_RESPONSE_TIME = BASE_URL + "ResponseTime.ashx";
  static const String URL_MODIFY_WOREPORT = BASE_URL + "addWokreport.ashx";
  static const String URL_CLOSE_WORKORDER = BASE_URL + "closeWorkorder.ashx";
  static const String URL_GET_CHILDREN_BY_PARENT =
      BASE_URL + "getChildrenByParent.ashx";
  static const String URL_CHANGE_EQUIPMENT_INFO =
      BASE_URL + "changeEquipmentInfo.ashx";
  static const String URL_ERP_CUSTORD_SERVICE =
      BASE_ERP_URL + "CustOrdService.ashx";

  ///登录请求
  static login(
      {@required String username,
      @required String password,
      @required Function loginSuccess,
      @required Function loginError}) {
    FormData params = FormData.from({
      'username': username,
      "password": EnDecodeUtil.encodeMd5(password).toUpperCase(),
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_LOGIN, (data) {
      LoginModel loginModel = LoginModel.fromJson(data);
      if (RESULT_OK == loginModel.result) {
        if (loginModel.authority != "1") {
          loginError("你不是机修工，没有权限登录");
          return;
        }
        SpUtil.putString(SP_TAG_USERNAME, username);
        SpUtil.putString(SP_TAG_PASSWORD, password);
        SpUtil.putString(SP_TAG_REALNAME, loginModel.realname);
        SpUtil.putString(SP_TAG_SITE, loginModel.site);
        SpUtil.putString(SP_TAG_CONTRACT_KEY, loginModel.siteKey);
        SpUtil.putString(SP_TAG_AUTHORITY, loginModel.authority);
        PushManager.getInstance().setAlias(username);
        loginSuccess();
      } else {
        loginError(loginModel.msg);
      }
    }, params: params);
  }

  ///获取报修列表请求
  static getRepairList({@required String username,
    @required String tag,
    @required String pageSize,
    @required String pageNum,
    @required Function getRepairListSuccess,
    @required Function getRepairListError}) {
    FormData params = FormData.from({
      'username': username,
      'tag': tag,
      'pageSize': pageSize,
      'pageNum': pageNum,
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_ARRAY, URL_REPAIR_LIST, (data) {
      List<RepairListModel> list = getRepairListModelList(data);
      getRepairListSuccess(list);
    }, params: params);
  }

  ///获取报修详情请求
  static getRepairDetails({@required String repairId,
    @required Function getRepairDetailsSuccess,
    @required Function getRepairDetailsError}) {
    FormData params = FormData.from({
      'repairId': repairId,
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_REPAIR_DETAILS,
            (data) {
      RepairDetailsModel repairDetailsModel = RepairDetailsModel.fromJson(data);
      if (RESULT_OK == repairDetailsModel.result) {
        getRepairDetailsSuccess(repairDetailsModel);
      } else {
        getRepairDetailsError(repairDetailsModel.msg);
      }
    }, params: params);
  }

  ///维修响应请求
  static responseTime({@required String username,
    @required String isWait,
    @required String repairId,
    @required String ResponseTimeLeft,
    @required Function responseTimeSuccess,
    @required Function responseTimeError}) {
    FormData params = FormData.from({
      'username': username,
      'isWait': isWait,
      'repairId': repairId,
      'ResponseTimeLeft': ResponseTimeLeft,
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_RESPONSE_TIME, (data) {
      NormalModel normalModel = NormalModel.fromJson(data);
      if (RESULT_OK == normalModel.result) {
        responseTimeSuccess();
      } else {
        responseTimeError(normalModel.msg);
      }
    }, params: params);
  }

  ///根据父节点获取子节点列表请求
  static getChildrenByParent({@required String username,
    @required String parent,
    @required String parentPath,
    @required String site,
    @required Function getChildrenByParentSuccess,
    @required Function getChildrenByParentError}) {
    FormData params = FormData.from({
      'username': username,
      'parent': parent,
      'parentPath': parentPath,
      'site': site,
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_GET_CHILDREN_BY_PARENT,
            (data) {
          print(data);
          ChildrenByParentModel childrenByParentModel =
          ChildrenByParentModel.fromJson(data);
          if (RESULT_OK == childrenByParentModel.result) {
            getChildrenByParentSuccess(childrenByParentModel.data);
          } else {
            getChildrenByParentError(childrenByParentModel.msg);
          }
        }, params: params);
  }

  ///修改设备信息请求
  static changeEquipmentInfo({@required String repairId,
    @required String EquipmentNameGroup,
    @required String EquipmentCodeGroup,
    @required String equipmentName,
    @required String equipmentCode,
    @required String inAttr,
    @required Function httpSuccess,
    @required Function httpError}) {
    FormData params = FormData.from({
      'username': SpUtil.getString(SP_TAG_USERNAME),
      'realname': SpUtil.getString(SP_TAG_REALNAME),
      'repairId': repairId,
      'EquipmentNameGroup': EquipmentNameGroup,
      'EquipmentCodeGroup': EquipmentCodeGroup,
      'equipmentName': equipmentName,
      'equipmentCode': equipmentCode,
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'inAttr': inAttr,
      'procedureName': "MODIFY_WOREPORT_FOR_APP",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_CHANGE_EQUIPMENT_INFO,
            (data) {
          print(data);
          NormalModel normalModel = NormalModel.fromJson(data);
          if (RESULT_OK == normalModel.result) {
            httpSuccess();
          } else {
            httpError(normalModel.msg);
          }
        }, params: params);
  }

  ///获取已执行活动列表请求
  static chooseActivity(
      {@required Function httpSuccess, @required Function httpError}) {
    FormData params = FormData.from({
      'inAttr': "{}",
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'userId': SpUtil.getString(SP_TAG_USERNAME),
      'procedureName': "QUERY_MAIN_PERF_ACTION",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_ERP_CUSTORD_SERVICE,
            (data) {
          print(data);
          String s = data.toString();
          ExecutedActivitiesModel executedActivitiesModel =
          ExecutedActivitiesModel.fromJson(data);
          if (RESULT_OK == executedActivitiesModel.successFlag) {
            httpSuccess(executedActivitiesModel);
          } else {
            httpError(executedActivitiesModel.eRRORMSG);
          }
        }, params: params);
  }

  ///获取逻辑设备请求
  static getItemClass({
    @required String inAttr,
    @required Function httpSuccess,
    @required Function httpError,
  }) {
    FormData params = FormData.from({
      'inAttr': inAttr,
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'userId': SpUtil.getString(SP_TAG_USERNAME),
      'procedureName': "QUERY_LOGIC_EQUIPMENT",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_ERP_CUSTORD_SERVICE,
            (data) {
          print(data);
          String s = data.toString();
          ItemClassModel itemClassModel = ItemClassModel.fromJson(data);
          if (RESULT_OK == itemClassModel.successFlag) {
            httpSuccess(itemClassModel);
          } else {
            httpError(itemClassModel.errorMsg);
          }
        }, params: params);
  }

  ///获取错误类型请求
  static getErrorType({
    @required String inAttr,
    @required Function httpSuccess,
    @required Function httpError,
  }) {
    FormData params = FormData.from({
      'inAttr': inAttr,
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'userId': SpUtil.getString(SP_TAG_USERNAME),
      'procedureName': "QUERY_ERR_TYPE",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_ERP_CUSTORD_SERVICE,
            (data) {
          print(data);
          String s = data.toString();
          ErrTypeModel errTypeModel = ErrTypeModel.fromJson(data);
          if (RESULT_OK == errTypeModel.successFlag) {
            httpSuccess(errTypeModel);
          } else {
            httpError(errTypeModel.errorMsg);
          }
        }, params: params);
  }

  ///获取原因请求
  static getReason({
    @required String inAttr,
    @required Function httpSuccess,
    @required Function httpError,
  }) {
    FormData params = FormData.from({
      'inAttr': inAttr,
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'userId': SpUtil.getString(SP_TAG_USERNAME),
      'procedureName': "QUERY_ERR_CAUSE",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_ERP_CUSTORD_SERVICE,
            (data) {
          print(data);
          String s = data.toString();
          ReasonModel reasonModel = ReasonModel.fromJson(data);
          if (RESULT_OK == reasonModel.successFlag) {
            httpSuccess(reasonModel);
          } else {
            httpError(reasonModel.errorMsg);
          }
        }, params: params);
  }

  ///填写工单报告
  static modifyWoreport({
    @required String inAttr,
    @required String errorType,
    @required String executedActivity,
    @required String reason,
    @required String repairId,
    @required String WO_NO,
    @required Function httpSuccess,
    @required Function httpError,
  }) {
    FormData params = FormData.from({
      'inAttr': inAttr,
      'errorType': errorType,
      'executedActivity': executedActivity,
      'reason': reason,
      'repairId': repairId,
      'WO_NO': WO_NO,
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'username': SpUtil.getString(SP_TAG_USERNAME),
      'realname': SpUtil.getString(SP_TAG_REALNAME),
      'procedureName': "MODIFY_WOREPORT_FOR_APP",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_MODIFY_WOREPORT,
            (data) {
          print(data);
          String s = data.toString();
          NormalModel normalModel = NormalModel.fromJson(data);
          if (RESULT_OK == normalModel.result) {
            httpSuccess();
          } else {
            httpError(normalModel.msg);
          }
        }, params: params);
  }

  ///关闭工单
  static closeWorkorder({
    @required String inAttr,
    @required String repairId,
    @required Function httpSuccess,
    @required Function httpError,
  }) {
    FormData params = FormData.from({
      'inAttr': inAttr,
      'repairId': repairId,
      'contract': SpUtil.getString(SP_TAG_SITE),
      'contractKey': SpUtil.getString(SP_TAG_CONTRACT_KEY),
      'username': SpUtil.getString(SP_TAG_USERNAME),
      'realname': SpUtil.getString(SP_TAG_REALNAME),
      'procedureName': "EDIT_WOREPORT_STATE",
    });
    HttpHelp.getInstance().post(PARSE_TYPE.JSON_OBJ, URL_CLOSE_WORKORDER,
            (data) {
          print(data);
          String s = data.toString();
          NormalModel normalModel = NormalModel.fromJson(data);
          if (RESULT_OK == normalModel.result) {
            httpSuccess();
          } else {
            httpError(normalModel.msg);
          }
        }, params: params);
  }
}
