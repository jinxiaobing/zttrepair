import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:zttrepair/app/app_component.dart';
import 'package:zttrepair/push/jpush/jpush_strategy.dart';
import 'package:zttrepair/push/push_manager.dart';
import 'package:zttrepair/push/push_strategy.dart';

Future main() async {
  await SpUtil.getInstance();

  runApp(AppComponent());
}
