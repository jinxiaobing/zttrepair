/*
 * fluro
 * Created by Yakka
 * https://theyakka.com
 * 
 * Copyright (c) 2019 Yakka, LLC. All rights reserved.
 * See LICENSE for distribution and usage details.
 */
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:zttrepair/ui/change_equipment_page.dart';
import 'package:zttrepair/ui/edit_failure_report_page.dart';
import 'package:zttrepair/ui/help_page.dart';
import 'package:zttrepair/ui/login_page.dart';
import 'package:zttrepair/ui/main_page.dart';
import 'package:zttrepair/ui/repair_details_page.dart';
import 'package:zttrepair/ui/repair_list_page.dart';
import 'package:zttrepair/ui/welcome_page.dart';

var rootHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return WelcomePage();
});
var loginHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return LoginPage();
});
var mainHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return MainPage();
    });
var helpHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return HelpPage();
    });
var repairListHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String title = params["title"]?.first;
      String tag = params["tag"]?.first;
      return RepairListPage(title: title, tag: tag);
    });
var repairDetailsHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String repairId = params["repairId"]?.first;
      return RepairDetailsPage(repairId: repairId);
    });
var changeEquipmentHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String equipmentNameGroup = params["equipmentNameGroup"]?.first;
      String equipmentNameGroupId = params["equipmentNameGroupId"]?.first;
      String repairId = params["repairId"]?.first;
      String orderId = params["orderId"]?.first;
      return ChangeEquipmentPage(
          equipmentNameGroup: equipmentNameGroup,
          equipmentNameGroupId: equipmentNameGroupId,
          repairId: repairId,
          orderId: orderId);
    });
var editFailureReportHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String MCH_CODE = params["MCH_CODE"]?.first;
      String repairId = params["repairId"]?.first;
      String orderId = params["orderId"]?.first;
      return EditFailureReportPage(orderId, repairId, MCH_CODE);
    });
