/*
 * fluro
 * Created by Yakka
 * https://theyakka.com
 * 
 * Copyright (c) 2019 Yakka, LLC. All rights reserved.
 * See LICENSE for distribution and usage details.
 */
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';

import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';
import 'package:zttrepair/config/colors.dart';
import 'package:zttrepair/push/jpush/jpush_strategy.dart';
import 'package:zttrepair/push/push_manager.dart';
import 'package:zttrepair/push/push_strategy.dart';

class AppComponent extends StatefulWidget {
  @override
  State createState() {
    return AppComponentState();
  }
}

class AppComponentState extends State<AppComponent> {
  AppComponentState() {
    final router = Router();
    Routes.configureRoutes(router);
    Application.router = router;
    PushStrategy strategy = new JPushStrategy(context);
    PushManager.getInstance().setStrategy(strategy);
  }

  @override
  Widget build(BuildContext context) {
    final app = MaterialApp(
      title: 'Fluro',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: color_default,
      ),
      onGenerateRoute: Application.router.generator,
    );
//    print("initial route = ${app.initialRoute}");
    return app;
  }
}
