import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:zttrepair/app/route_handlers.dart';


class Routes {
  static String root = "/";
  static String login = "/login";
  static String main = "/main";
  static String help = "/help";
  static String repair_list = "/repair_list";
  static String repair_details = "/repair_details";
  static String change_equipment = "/change_equipment";
  static String edit_failure_report = "/edit_failure_report";

  static void configureRoutes(Router router) {
    router.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      print("ROUTE WAS NOT FOUND !!!");
    });
    router.define(root, handler: rootHandler);
    router.define(login,
        handler: loginHandler, transitionType: TransitionType.inFromRight);
    router.define(main,
        handler: mainHandler, transitionType: TransitionType.inFromRight);
    router.define(help,
        handler: helpHandler, transitionType: TransitionType.inFromRight);
    router.define(repair_list,
        handler: repairListHandler, transitionType: TransitionType.inFromRight);
    router.define(repair_details,
        handler: repairDetailsHandler,
        transitionType: TransitionType.inFromRight);
    router.define(change_equipment,
        handler: changeEquipmentHandler,
        transitionType: TransitionType.inFromRight);
    router.define(edit_failure_report,
        handler: editFailureReportHandler,
        transitionType: TransitionType.inFromRight);
  }
}
