import 'package:flutter/material.dart';

abstract class PushStrategy {
  void initPush(BuildContext context);

  void setAlias(String alias);

  void deleteAlias();
}
