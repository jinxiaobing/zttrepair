import 'package:zttrepair/push/push_strategy.dart';

class PushManager {
  _PushManager() {}

  static PushManager getInstance() {
    return PushManagerHolder.sInstance;
  }

  PushStrategy mStragety;

  void setStrategy(PushStrategy stragety) {
    this.mStragety = stragety;
  }

  void setAlias(String alias) {
    mStragety.setAlias(alias);
  }

  void deleteAlias() {
    mStragety.deleteAlias();
  }
}

class PushManagerHolder {
  static final PushManager sInstance = new PushManager();
}
