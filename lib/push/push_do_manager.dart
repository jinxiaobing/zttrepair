import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:zttrepair/app/routes.dart';
import 'package:zttrepair/config/application.dart';

class PushDoManager {
  static void processCustomMessage(BuildContext context) {
    //解锁屏幕
    _unlockScreen();
    //打开或刷新报修列表
    _openOrRefrshRepairList(context);
    //震动响铃
    _vibrationAndRing();
  }

  static void _unlockScreen() {}

  static void _openOrRefrshRepairList(BuildContext context) {
    String title = jsonEncode(Utf8Encoder().convert("待响应"));
    String path = '${Routes.repair_list}?title=$title&tag=0';
    Application.router.navigateTo(context, path);
  }

  static void _vibrationAndRing() {}
}
