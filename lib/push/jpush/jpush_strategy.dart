import 'package:flutter/material.dart';
import 'package:zttrepair/push/push_do_manager.dart';
import 'package:zttrepair/push/push_strategy.dart';
import 'package:jpush_flutter/jpush_flutter.dart';

class JPushStrategy implements PushStrategy {
  final JPush jpush = new JPush();

  JPushStrategy(BuildContext context) {
    initPush(context);
  }

  @override
  Future<void> initPush(BuildContext context) async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    jpush.getRegistrationID();
    jpush.setup(
      appKey: "1887902d18335bbf2085002e",
      channel: "theChannel",
      production: false,
      debug: true,
    );
    jpush.applyPushAuthority(
        new NotificationSettingsIOS(sound: true, alert: true, badge: true));
    jpush.addEventHandler(
      onReceiveNotification: (Map<String, dynamic> message) async {
        print("flutter onReceiveNotification: $message");
      },
      onOpenNotification: (Map<String, dynamic> message) async {
        print("flutter onOpenNotification: $message");
      },
      onReceiveMessage: (Map<String, dynamic> message) async {
        print("flutter onReceiveMessage: $message");
        PushDoManager.processCustomMessage(context);
      },
    );
  }

  @override
  void setAlias(String alias) {
    jpush.setAlias(alias);
  }

  @override
  void deleteAlias() {
    jpush.deleteAlias();
  }
}
